<?php
/**
 * ExchangeTest
 *
 * PHP version 5
 *
 * @category Class
 * @package  PushkaExchange\Client
 * @author   Swagger Codegen team
 * @link     https://github.com/swagger-api/swagger-codegen
 */

/**
 * Обмен идентификаторами
 *
 * API для обмена идентификаторами системы «PRO.Культура.РФ». Токен для тестирования – i7mrk134sklozq8uhed7
 *
 * OpenAPI spec version: 0.1
 * 
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 * Swagger Codegen version: 3.0.29
 */
/**
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen
 * Please update the test case below to test the model.
 */

namespace PushkaExchange\Client;

/**
 * ExchangeTest Class Doc Comment
 *
 * @category    Class
 * @description Exchange
 * @package     PushkaExchange\Client
 * @author      Swagger Codegen team
 * @link        https://github.com/swagger-api/swagger-codegen
 */
class ExchangeTest extends \PHPUnit_Framework_TestCase
{

    /**
     * Setup before running any test case
     */
    public static function setUpBeforeClass()
    {
    }

    /**
     * Setup before running each test case
     */
    public function setUp()
    {
    }

    /**
     * Clean up after running each test case
     */
    public function tearDown()
    {
    }

    /**
     * Clean up after running all test cases
     */
    public static function tearDownAfterClass()
    {
    }

    /**
     * Test "Exchange"
     */
    public function testExchange()
    {
    }

    /**
     * Test attribute "system_inn"
     */
    public function testPropertySystemInn()
    {
    }

    /**
     * Test attribute "system_event_id"
     */
    public function testPropertySystemEventId()
    {
    }

    /**
     * Test attribute "pro_event_id"
     */
    public function testPropertyProEventId()
    {
    }

    /**
     * Test attribute "status"
     */
    public function testPropertyStatus()
    {
    }
}
