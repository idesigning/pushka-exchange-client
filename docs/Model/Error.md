# Error

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**error** | **string** | Код ошибки | 
**error_name** | **string** | Системное описание ошибки | 
**user_message** | **string** | Описание ошибки для пользователя | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

