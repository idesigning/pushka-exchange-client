# Exchange

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**system_inn** | **string** | ИНН системы, которая регистрирует мероприятие | 
**system_event_id** | **string** | Внутренний идентификатор сеанса в системе, которая регистрирует мероприятие | 
**pro_event_id** | **string** | Идентификатор мероприятия в системе «PRO.Культура.РФ» | 
**status** | **string** | Статус доступности мероприятия к участию в проекте «Пушкинская карта» у организации-владельца (available – доступно, not available – недоступно) | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

