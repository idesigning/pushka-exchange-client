# PushkaExchange\Client\_Api

All URIs are relative to *https://pro.culture.ru/api/v2.5/pushka?apiKey&#x3D;i7mrk134sklozq8uhed7*

Method | HTTP request | Description
------------- | ------------- | -------------
[**idexchangePost**](_Api.md#idexchangepost) | **POST** /idexchange | Связка ID мероприятия в «PRO.Культура.РФ» с внутренним ID билетной системы
[**idexchangeSystemInnSystemEventIdGet**](_Api.md#idexchangesysteminnsystemeventidget) | **GET** idexchange/{systemInn}/{systemEventId} | Получить ID мероприятия «PRO.Культура.РФ» по ИНН билетной системы и ID мероприятия в билетной системе

# **idexchangePost**
> idexchangePost($body)

Связка ID мероприятия в «PRO.Культура.РФ» с внутренним ID билетной системы

После внесения идентификатора мероприятия из системы «PRO.Культура.РФ» данные об этом передаются в систему обмена идентификаторами. После связи по шлюзу билетный оператор регистрирует своё мероприятие в системе обмена идентификаторами

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new PushkaExchange\Client\Api\_Api(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$body = new \PushkaExchange\Client\Model\Exchange(); // \PushkaExchange\Client\Model\Exchange | 

try {
    $apiInstance->idexchangePost($body);
} catch (Exception $e) {
    echo 'Exception when calling _Api->idexchangePost: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**\PushkaExchange\Client\Model\Exchange**](../Model/Exchange.md)|  | [optional]

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **idexchangeSystemInnSystemEventIdGet**
> \PushkaExchange\Client\Model\Exchange idexchangeSystemInnSystemEventIdGet($system_inn, $system_event_id)

Получить ID мероприятия «PRO.Культура.РФ» по ИНН билетной системы и ID мероприятия в билетной системе

Система, получившая места по шлюзу или по квоте, на основании идентификатора системы-источника и ИНН системы-владельца получает идентификатор события в «PRO.Культура.РФ».

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new PushkaExchange\Client\Api\_Api(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$system_inn = "system_inn_example"; // string | ИНН системы, от которой получены билеты. Для системы-организатора — собственный
$system_event_id = "system_event_id_example"; // string | Идентификатор сеанса в системе, от которой получены билеты. Для системы-организатора — собственный

try {
    $result = $apiInstance->idexchangeSystemInnSystemEventIdGet($system_inn, $system_event_id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling _Api->idexchangeSystemInnSystemEventIdGet: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **system_inn** | **string**| ИНН системы, от которой получены билеты. Для системы-организатора — собственный |
 **system_event_id** | **string**| Идентификатор сеанса в системе, от которой получены билеты. Для системы-организатора — собственный |

### Return type

[**\PushkaExchange\Client\Model\Exchange**](../Model/Exchange.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

